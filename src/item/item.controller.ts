import { Controller, Get, Post, Put, Delete, Request, Response, Body, Param } from '@nestjs/common';
import { ItemService } from './item.service';
import { Item } from './item.entity';
@Controller('items')
export class ItemController {
    constructor(private readonly itemService: ItemService) {}

    @Get()
    getAll(): Promise<Item[]> {
      return this.itemService.getAll();
    }

    @Post()
    create(@Body() item: Item) {
        return this.itemService.create(item)
    }

    @Get(':itemId')
    getSingle(@Param('itemId') itemId): Promise<Item[]> {
        return this.itemService.getSingle(itemId)
    }

    @Put(':itemId')
    update(@Param('itemId') itemId, @Body() item: Item) {
        return this.itemService.update(itemId, item);
    }

    @Delete(':itemId')
    remove(@Param('itemId') itemId) {
        return this.itemService.remove(itemId);
    }
}
