import { Entity, Column, PrimaryGeneratedColumn, JoinColumn, ManyToOne } from 'typeorm';
import { Item } from './item.entity';

@Entity()
export class ItemRelative {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    item_id: number;

    @Column()
    history: string;

    @ManyToOne(() => Item, (item: Item) => item.itemrelatives)
    @JoinColumn({name: 'item_id'})
    item: Item;
}