import { Entity, Column, PrimaryGeneratedColumn, OneToMany, JoinColumn } from 'typeorm';
import { ItemRelative } from './item-relative.entity';

@Entity()
export class Item {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    description: string;

    @Column()
    price: number;

    @Column()
    quantity: number;

    @OneToMany(type => ItemRelative, itemRelative => itemRelative.item)
    itemrelatives: ItemRelative[];
}