import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Item } from './item.entity';

@Injectable()
export class ItemService {
    constructor(
    @InjectRepository(Item)
        private readonly itemRepository: Repository<Item>,
    ) {}
    
    async getAll(): Promise<Item[]> {
        return await this.itemRepository.find();
    }

    async create(item: Item) {
        return this.itemRepository.save(item)
    }

    async getSingle(itemId): Promise<Item[]> {
        return await this.itemRepository.find({ 
            relations: ['itemrelatives'],
            where: { "id": itemId }})
    }
    
    async update(itemId, item: Item) {
        return this.itemRepository.update(itemId, item);
    }
    
    async remove(itemId) {
        return this.itemRepository.delete(itemId);
    }
}
